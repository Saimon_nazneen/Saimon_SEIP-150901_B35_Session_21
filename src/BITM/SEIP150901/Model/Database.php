<?php
namespace App\Model;

use PDO;
use PDOException;
// print_r(PDO::getAvailableDrivers());  //ড্রাইভার আছে তা দেখতে  কোড ;


class Database{

    public $DBH;
    public $host = "localhost";
    public $dbname = "atomicproject";
    public $user = "root";
    public $pass = "";

    public function __construct()
    {
        try {
            # MySQL with PDO_MYSQL
            $DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname",
                                       $this->user, $this->pass);
            echo "connected Successfully";
        }
        catch(PDOException $e)
            {
            echo $e->getMessage();
            }

    }
}
//$objDatabe = new Database();